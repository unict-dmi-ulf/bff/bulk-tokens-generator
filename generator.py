from flask import Flask
from flask import request
import struct
import os
import requests
import qrcode
from flask import send_file
from fpdf import FPDF
import random
import string

app = Flask(__name__)


def clearDir():
    dir = 'temp/'
    for f in os.listdir(dir):
        os.remove(os.path.join(dir, f))

def get_random_string(length):
    # choose from all lowercase letter
    letters = string.ascii_lowercase
    return ''.join(random.choice(letters) for i in range(length))
   

@app.route("/generator", methods=['GET'])
def addTokens():
    
    course = request.args.get('course')
    expDate = request.args.get('expDate')
    pdf = FPDF()
    for idx, token in enumerate(range(0, 100)):
        newToken = get_random_string(16)
        myjson = {
            "accessibleCourses": [
                {
                    "course": course,
                }
            ], 
            "expirationDate": expDate,
            "value": newToken
        }

        x=requests.post('http://127.0.0.1:9012/tokens', json=myjson)
       
        if(x.status_code == 201) :
            token = qrcode.make(newToken)
            token.convert('RGB')

            token.save('temp/qrcode_'+str(idx)+'.png')
            pdf.add_page()
            pdf.image('temp/qrcode_'+str(idx)+'.png', 50, 50)
        else:
            return "Internal server error", 500

    pdf.output("newTokens.pdf", "F")
    clearDir()

    return send_file("newTokens.pdf", as_attachment=True)
