from flask import Flask
from flask import request
import struct
import os
import qrcode
import img2pdf
from flask import send_file
from fpdf import FPDF

app=Flask(__name__)

def clearDir():
    dir = 'temp/'
    for f in os.listdir(dir):
        os.remove(os.path.join(dir, f))


@app.route("/generator/generateTokens", methods=['GET'])
def addTokens():
    
    pdf = FPDF()

    for idx,token in enumerate(range(0,100)):
        token=qrcode.make(str(struct.unpack('d', os.urandom(8))))
        token.convert('RGB')
        token.save('temp/qrcode_'+str(idx)+'.png')
        pdf.add_page()
        pdf.image('temp/qrcode_'+str(idx)+'.png',50,50)
        
    pdf.output("newTokens.pdf", "F")
    clearDir()
        
    return  send_file("newTokens.pdf",as_attachment=True)